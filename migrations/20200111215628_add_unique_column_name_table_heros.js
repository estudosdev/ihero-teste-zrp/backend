
exports.up = function(knex) {
  return knex.schema
    .alterTable('heros', (table) => {
        table.unique('name')
    })
};

exports.down = function(knex) {
    return knex.schema
        .alterTable('heros', (table) => {
            table.dropUnique('name')
        })
};
