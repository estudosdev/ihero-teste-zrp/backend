
exports.up = function(knex) {
    return knex.raw('ALTER TABLE users RENAME COLUMN name TO username')
};

exports.down = function(knex) {
    return knex.raw('ALTER TABLE users RENAME COLUMN username TO name')
};
