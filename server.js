require('dotenv').config()
const http = require('http')
const app = require('./src/app')
const port = process.env.PORT_APLICATION
const hostname = '0.0.0.0'

const server = http.createServer(app)
server.listen(port, hostname, () => {
    console.log(`Server is running http://${hostname}:${port}`)
})