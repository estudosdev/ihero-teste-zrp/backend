const requets = require('supertest')
const app = require('../../src/app')

describe('Tasks', () => {    
    describe('/POST auth middleware', () => {
        it('caso o usuario nao sete o Authorization no header', async (done) => {
            const res = await requets(app).post('/')
            expect(res.status).toBe(401)
            expect(res.body).toHaveProperty('Error', 'Token not found')
            done()
        })
        it('Json bad formatedd', async (done) => {
            const res = await requets(app).post('/').set('Authorization', 'tokeasdsan pokasopda poaksodkoas')
            expect(res.status).toBe(401)
            expect(res.body).toHaveProperty('Error', 'Json bad formatedd')
            done()
        })
        it('Token invalid or experired', async (done) => {
            const res = await requets(app).post('/').set('Authorization', 'Bearer aoskfaopkfop')
            expect(res.status).toBe(401)
            expect(res.body).toHaveProperty('Error', 'Token invalid or experired')
            done()
        })
    })
})

