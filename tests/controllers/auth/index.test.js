const requets = require('supertest')
const app = require('../../../src/app')
const tableNames = require('../../helpers/tables')
const dbInstance = require('../../../src/config/knex')

beforeAll(() => {    
    tableNames.map( async (table) => await dbInstance.queryBuilder().table(table).delete() )
})

describe('Tasks Auth', () => {    
    describe('/POST register', () => {        
        it('Create user success', async (done) => {
            const body = { username: 'matheus.souza', password: '1234' }
            const res = await requets(app).post('/auth/register').send(body)
            expect(res.status).toBe(200)
            done()
        })
        it('Create user duplicate', async (done) => {
            const body = { username: 'matheus.souza', password: '1234' }            
            const res = await requets(app).post('/auth/register').send(body)
            expect(res.status).toBe(400)
            done()
        })
    })    
    describe('/POST login', () => {
        it('Login success', async (done) => {
            const body = { username: 'matheus.souza', password: '1234' }
            const res = await requets(app).post('/auth/login').send(body)
            expect(res.status).toBe(200)
            done()
        })
        it('Password or Username undefined', async (done) => {
            const body = { username: 'matheus.souza'}
            const res = await requets(app).post('/auth/login').send(body)
            expect(res.status).toBe(401)
            expect(res.body).toHaveProperty('Error')
            done()
        })
    })
})

