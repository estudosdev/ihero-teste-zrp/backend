const request = require('supertest')
const app = require('../../../src/app')
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTY0LCJpYXQiOjE1Nzg4NjAwNzQsImV4cCI6MTU3ODk0NjQ3NH0.DtGZ2gxrhJtaiBfYrsk4mNcy6a4mogJL75EG3LS98MQ'

describe('Task CRUD Hero', () => {
    describe('/POST create new hero', () => {
        it('Name is required', async (done) => {
            const res = await request(app).post('/hero').set('Authorization', `Bearer ${token}`)
            expect(res.status).toBe(400)
            done()
        })
        // it('success in create one hero', async (done) => {
        //     const res = await request(app).post('/hero')
        // })
    })
})