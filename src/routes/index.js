const authMiddleware = require('../middleware/auth')
const authRouter = require('./auth')
const heroRouter = require('./hero')

module.exports = (app) => {    
    app.use('/auth', authRouter)
    
    app.use(authMiddleware)
    app.use('/hero', heroRouter)
}