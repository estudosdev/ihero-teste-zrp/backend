const router = require('express').Router()
const heroController = require('../controllers/hero')

router.post('/', heroController.create)
router.get('/', heroController.getAll)
router.get('/:id', heroController.getById)
router.put('/:id', heroController.updateById)
router.delete('/:id', heroController.deleteById)

module.exports = router