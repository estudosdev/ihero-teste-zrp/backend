require('dotenv').config()
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const dbInstance = require('../../config/knex')
const SECRETE_KEY_JWT = process.env.SECRETE_KEY_JWT
const expiresIn = 86400

module.exports = {
    register: (req, res) => {
        try {
            const { username, password } = req.body
            if(!username || !password)
                return res.status(401).json({ 'Error': 'username and password is not null' })

            const hashPassword = bcrypt.hashSync(password, 8)
            const user = { username, password: hashPassword }            
                        
            dbInstance.queryBuilder().table('users').insert(user).returning('id')
                .then(id => {
                    token = jwt.sign({id}, SECRETE_KEY_JWT, {
                        expiresIn
                    })
                    
                    delete user.password
                    return res.status(200).json({ 'Token': token, 'User': user })                    
                })
                .catch(e => res.status(400).json({ 'Error': e }))
        } catch (error) {
            return res.status(500).json({ 'Error': 'nothing processed' })
        }
    },
    login: (req, res) => {
        try {            
            const { username, password } = req.body

            if(!username || !password)
                return res.status(401).json({ 'Error': 'username and password is not null' })

            dbInstance.queryBuilder().table('users').where({ username }).first().returning('*')
                .then(user => {
                    if(!bcrypt.compareSync(password, user.password))
                        return res.status(401).json({ 'Error': 'Invalid credentials'})
                    
                    token = jwt.sign({ id: user.id }, SECRETE_KEY_JWT, {
                        expiresIn
                    })

                    delete user.password
                    return res.status(200).json({
                        'Token': token,
                        'Message': 'Login realized with success',
                        'User': user
                    })
                })
                .catch(e => res.status(400).json({ 'Error': e }))
        } catch (error) {            
            return res.status(500).json({ 'Error': 'nothing processed' })
        }
    }
}