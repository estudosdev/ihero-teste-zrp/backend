const dbInstance = require('../../config/knex')

module.exports = {
    create: async (req, res) => {        
        try {
            const { name } = req.body
            
            if(!name) return res.status(400).json({ 'Error': 'Name is required' })

            const [ hero ] = await dbInstance.queryBuilder().table('heros').insert({ name }).returning('*')
            
            return res.status(200).json({ 'Hero': hero })
        } catch (error) {
            if(error.detail && error.detail === `Key (name)=(${req.body.name}) already exists.`) 
                return res.status(400).json({ 'Error': error })
            
            return res.status(400).json({ 'Error': 'error' })
        }
    },    
    getAll: async (_, res) => {
        try {
            var heros = await dbInstance.queryBuilder().table('heros').select('*').where('id', 11)
        } catch (error) {
            return res.status(400).json({ 'Error': 'error' })
        } finally {            
            return res.status(200).json({ 'Heros': heros })
        }
    },
    getById: async (req, res) => {
        try {
            const { id } = req.params
            var hero = await dbInstance.queryBuilder().table('heros').select('*').where('id', id).first()
        } catch (error) {
            return res.status(400).json({ 'Error': 'error' })
        } finally {
            return res.status(200).json({ 'Hero': hero })
        }
    },
    updateById: async (req, res) => {
        try {
            const { id } = req.params
            const { name } = req.body            
            if (!name || name === '') return res.status(500).json({ 'Error': 'Field name is not null' })

            var [ hero ] = await dbInstance.queryBuilder().table('heros').update({ 'name': name }).where('id', id).returning('*')
        } catch (error) {            
            return res.status(400).json({ 'Error': 'error' })
        } finally {
            return res.status(200).json({
                Message: 'Updated with success',
                Hero: hero
            })
        }
    },
    deleteById: async (req, res) => {
        try {
            const { id } = req.params                                   
            await dbInstance.queryBuilder().table('heros').del().where('id', id)
        } catch (error) {            
            return res.status(400).json({ 'Error': 'error' })
        } finally {
            return res.status(200).json({ Message: 'Deleted with success' })
        }
    }
}