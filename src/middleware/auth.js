const jwt = require('jsonwebtoken')
const SECRETE_KEY_JWT = process.env.SECRETE_KEY_JWT
const expiresIn = 86400

module.exports = (req, res, next) => {
    const authHeader = req.headers.authorization
    
    if(!authHeader) return res.status(401).json({ 'Error': 'Token not found' })

    const parts = authHeader.split(' ')
    
    if(parts.length < 2 || parts.length > 2) return res.status(401).json({ 'Error': 'Json bad formatedd' })
    
    const scheme = parts[0]    
    const token = parts[1]

    if(scheme !== 'Bearer') return res.status(401).json({ 'Error': 'Json bad formatedd' })

    try {
        jwt.verify(token, SECRETE_KEY_JWT, (err, decoded) => {
            if (err) return res.status(401).json({ 'Error': 'Token invalid or experired' })            
        })
    } catch (error) {
        return res.status(400).json({ 'Error': error })
    } finally {
        next()
    }
}